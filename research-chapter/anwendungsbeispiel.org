#+TITLE: A research chapter
#+LATEX_CLASS: elsarticle
#+LATEX_CLASS_OPTIONS: [authoryear,preprint,5p,times,twocolumn,11pt]
#+SETUPFILE: manuscript.setup
#+LATEX_HEADER:\bibliography{./ref/bibliography}
* Anwendungsbeispiel
Wie die verschiedenen Modelle praktisch funktionieren, soll nun an einem
Anwendungsbeispiel durchgespielt werden.
Aufgrund der aktuellen Lage und der Debatte darum, bietet sich die Entwicklung
einer Corona-App an. Die App soll dabei helfen, Infektionsketten
nachzuvollziehen und somit zur Eindämmung der Pandemie beitragen.
Ziel dieses Kapitels ist dabei nicht, eine derartige App zu entwickeln, sondern
die verschiedenen Herangehensweisen und Eigenschaften der einzelnen Modelle
herauszuarbeiten und zu vergleichen.
Da die App verschiedene Funktionalitäten vorsieht, von mehreren Plattformen
unterstützt und möglichst schnell realisiert werden soll, wird auf agile
Skalierung gesetzt. Dazu kommt die sehr dynamische Entwicklung des Umfelds, an
das die Entwicklung der Applikation angepasst sein sollte.
** SAFe
In diesem Abschnitt wird das Anwendungsbeispiel unter der Verwendung von SAFe
betrachtet.\\
Da die App aufgrund der aktuellen Situation schnell entwickelt werden soll,
stehen kurzfristig nur 41 Personen zur Verfügung, welche von anderen Projekten
abgezogen wurden.
Diese Personen werden auf vier Entwicklerteams mit jeweils einem eigenen Scrum
Master sowie einem Release Train Engineer aufgeteilt.
Pro Entwicklerteam stehen somit neun Mitarbeiter als Entwickler zur Verfügung.
Das oben aufgesetzte Portfoliolevel besteht aus den Personen der Führungsebene
auf die hier nicht weiter eingegangen wird.
Weiterhin ist nur die Entwicklung der App mit SAFe dargestellt, Bereiche wie
Marketing und Vertrieb werden nicht beachtet.
Im Portfoliolevel wird zuerst ein neues Programmlevel gestartet, welchem die
dort mitarbeitenden Personen zugeteilt werden.
Anschließend werden die zu erledigenden Aufgaben festgestellt und an das
Programmlevel übergeben.
Hier werden diese auf kleinere Teilbereiche aufgeteilt, die von den Teams in den
einzelnen Sprints abgearbeitet werden.
Da diese Teams in der Organisationsstruktur bereits bestehen, besitzen diese
bereits Kenntnisse über einzelne Aufgabenbereiche wie beispielsweise die
Entwicklung für Android- oder iOS-Geräte und fungieren somit als Experten.
Hierdurch können die Aufgaben so verteilt werden, dass jedes Team einzelne
Aufgaben bearbeitet, welche zu den Fachkenntnissen des jeweiligen Teams
passen.\\
In diesem Anwendungsbeispiel wird ein Team primär die Aufgaben der iOS- und ein
Team die der Android-Entwicklung übernehmen. Zudem wird es Aufgaben bzgl.
Webservices und Infrastruktur geben, welche von jeweils einem weiteren Team
primär bearbeitet werden.
Zur Aufgabenverteilung bespricht zunächst jedes Team intern, welche Aufgaben am
besten zu den jeweiligen Fachkenntnissen der Teammitglieder passen. Anschließend
erfolgen die Absprache der Teams untereinander sowie letztendlich die endgültige
Zuweisung der Aufgaben durch den Release Train Engineer.
Nach erfolgter Zuweisung, werden die Aufgaben in Sprints verteilt und in den
einzelnen Teams unter Anwendung von Daily Scrum Meetings selbstständig
bearbeitet.
Am Ende jedes Sprints findet die Retrospective statt.
Im Anschluss startet direkt ein neuer Sprint, in dem das Team weitere Aufgaben
abarbeitet.
Mit dem Ende des vierten Sprintes wird ein HIP-Sprint durchgeführt, an dem alle
Teams beteiligt sind.
Hierbei wird beispielsweise der aktuelle Entwicklungsstand der App präsentiert
und ein Hackathon für die Mitarbeiter veranstaltet.
Mit dem ersten HIP-Sprint endet auch der erste Release Train, woraus die erste
Version der App hervorgeht.
Diese kann nun zur Verfügung gestellt werden.
Im nächsten Release Train wird diese App durch neue Features ergänzt,
weiterentwickelt und verbessert.
** LeSS
Da die App möglichst schnell fertiggestellt werden soll, arbeiten 40 Mitarbeiter
in sechs Teams zusammen.
In diesem Beispiel wird nicht auf das gesamte Unternehmen eingegangen, es wird
nur der entwicklungsrelevante Teil betrachtet.\\
Die Teams werden zur Aufteilung der Themen in zwei Areas aufgeteilt.
Ein Area arbeitet an der Realisierung der App, während die zweite Area sich um
den rechtlichen Teil (Law-Area) kümmert.
Diese Einteilung wird trotz der geringen Anzahl an Mitarbeitern genutzt, um eine
sinnvolle Aufteilung der Themen vornehmen zu können.
In der Developer-Area kümmern sich drei Teams um iOS, Android und Webservices.
Auch im Law-Area arbeiten drei Teams, die unter anderem für die Zusammenarbeit
mit der Gesundheitsbehörde Sorge tragen.
Somit ergibt sich zusätzlich zu den 35 Teammitgliedern die Rollenverteilung für
zwei Area Product Owner, ein Product Owner und zwei Scrum Master.
Daraus folgt die Zusammensetzung der sechs Teams. In fünf der sechs Teams sind
sechs Mitglider und in einem fünf.
Da LeSS eine nahe Zusammenarbeit der Teams empfiehlt, können auf Wunsch der
Teammitglieder Communities mit Mentoren gebildet werden.
Wo es sinnvoll ist, werden auch teamübergreifende Daily Scrums und Product
Backlog Refinements abgehalten.
Die Optimierung der Prozesse wird am Ende der Sprints in Form der
teamübergreifenden und teaminternen Retrospektiven abgehalten.
Im Product Backlog werden die notwendigen Items definiert, die die jeweiligen
Area Product Backlogs weiter spezifizieren.
Die Aufteilung der Area Product Backlog Items geschieht autonom in den Teams.
Zu beachten ist, dass jedes Item aus dem Product Backlog mindestens ein Item im
passenden Area Product Backlog hat.
Umgekehrt lässt sich jedes Item aus dem Area Product Backlog auf eines aus dem
Product Backlog zurückführen.
Beim Refinement Prozess kann es geschehen, dass die im ersten Schritt
eingeplante Bearbeitungszeit neu definiert wird. Liegt der Zeitunterschied in
einem gewissen Rahmen, muss die Zeitplanung nicht neu koordiniert werden.
Zusammen mit der Ausarbeitung des Product Backlog wird vor Beginn des ersten
Sprints gemeinsam eine Definition of Done erstellt. Diese wird von Sprint zu
Sprint neu angepasst. So soll die richtige Priorisierung und Zielsetzung in der
aktuellen Situation dargestellt werden.
Da es in diesem Fall um ein relativ kleines Projekt geht, welches jedoch schnell
umgesetzt werden soll, wird das Ampel-System genutzt.
Beim Ampel-System wird der allseits bekannt Farbcode einer Ampel genutzt. Dabei
steht Rot für nicht angefangen, Gelb für Work In Progress (WIP) und Grün für
fertiggestellt.
Es ist relativ einfach zu handhaben und bietet die Möglichkeit, sich schnell
einen Überblick zum Status Quo der Entwicklung zu verschaffen. Dadurch bietet es
vergleichsweise viel für einen geringen Overhead.
** Spotify
In diesem Kapitel soll das bereits vorgestellte Anwendungsbeispiel unter
Anwendung des Spotify-Modells betrachtet werden.
Da die App aufgrund der Pandemie möglichst schnell zur Verfügung stehen soll,
ist trotz der überschaubaren Komplexität der Einsatz von 40 Mitarbeitern
vorgesehen.
Diese sind nun entsprechend auf Squads zu verteilen.
Es sei an dieser Stelle angemerkt, dass auf weitere für Unternehmen essentielle
Bereiche wie HR, Marketing, usw. nicht eingegangen wird.\\
Die Einführung des Spotify-Modells lässt sich bei Bedarf analog vornehmen, wobei
allerdings zu beachten ist, dass dieses hauptsächlich in der Softwareentwicklung
zum Einsatz kommt.
Die App soll auf iOS sowie Android Geräten eingesetzt werden können.
Sinnvollerweise kümmert sich um jede der beiden Plattformen jeweils ein Squad.
Es wird jedoch auch ausgelagerte Funktionalitäten geben, welche beispielsweise durch Webservices realisiert werden.
Auch hierfür wird ein Squad benötigt.
Damit sich die anderen Squads möglichst auf ihre Hauptaufgaben konzentrieren
können, kann ein weiterer Squad sich um Themen wie Continuous Delivery,
Continuous Integration oder Automated Testing kümmern, um den anderen Squads die
Entwicklung zu erleichtern.
Nicht zuletzt muss sich ein Squad mit den rechtlichen Themen wie Datenschutz
auseinander setzen.\\
Um eine möglichst schnelle Entwicklung der App zu ermöglichen, werden die
entwickelnden Squads sowie der für die Infrastruktur verantwortliche Squad
möglichst groß gewählt.
Hinsichtlich Datenschutz bestehen bereits Vorgaben, deswegen kann dieser Squad
etwas kleiner ausfallen.
Den entwickelnden Squads werden jeweils neun Mitarbeiter zugeteilt.
Es verbleiben vier Mitarbeiter, die sich um die rechtlichen Themen kümmern.
Von diesen neun bzw. vier Mitarbeitern, stellt jeweils einer den Product Owner
dar.
Er erstellt in Absprache mit den anderen Product Ownern das Backlog.
Da die Anzahl der Squads recht überschaubar ist, wird in diesem Beispiel auf
Tribes verzichtet.
Es könnte ohnehin nicht überall eine sinnvolle thematische Gruppierung
vorgenommen werden.
Das Spotify Modell lebt von stetigen Anpassungen:
Tribes weg zu lassen ist somit völlig legitim, wenn diese aufgrund der
vergleichsweise geringen Anzahl an Mitarbeitern nicht benötigt werden.\\
Eine Aufteilung der Mitarbeiter, wie oben beschrieben, ist dann sinnvoll, wenn
das Unternehmen nur über ein einziges Produkt verfügt und dabei am Anfang der
Entwicklung steht.
Zu Beginn ist eine Entwicklungsinfrastruktur aufzubauen.
Ebenso muss initial ein hoher Aufwand erbracht werden, um das plattformabhängige
Grundgerüst für die App zu erstellen.
Ist das Produkt bereits auf dem Markt, wird man sich verstärkt auf die
Entwicklung neuer Features sowie Verbesserungen konzentrieren.
Dadurch wird ein weiterer Squads zur Entwicklung dieser Features notwendig.
Gleichzeitig wird sich der Arbeitsaufwand in den Bereichen Infrastruktur sowie
den beiden Plattformen reduzieren, wodurch sich diese Squads verkleinern lassen.
So können beispielsweise von den drei  Squads, die hauptsächlich unterstützend
wirken, jeweils drei Mitarbeiter abgezogen werden, die dann den neuen Feature
Squad bilden.
Dies setzt selbstverständlich voraus, dass die Mitarbeiter auch über die
entsprechenden Fachkenntnisse verfügen.
Eine derartige Aufteilung kann auch dann sinnvoll sein, wenn das Unternehmen
über zwei Produkte verfügt.
Dabei übernimmt jeweils ein Feature Squad primär Features für
@@latex:\enquote{sein}@@ Produkt.
Mit zunehmendem Wachstum kämen weitere Squads hinzu, wobei sich ab einer
gewissen Größe eine sinnvolle Zusammenfassung zu Tribes ergeben würde.\\
Zurück zum Anwendungsbeispiel: Der Infrastructure Squad kümmert sich um
Continuous Delivery sowie Automated Testing, um den entwickelnden Squads den
Deployment-Prozess möglichst zu vereinfachen.
Die beiden plattformspezifischen Squads entwickeln den Rahmen der App und
integrieren bereits entwickelte Features.
Sie ermöglichen dem anderen Squad zudem die Continuous Integration.
Die plattformspezifischen Squads übernehmen also die Rolle der
@@latex:\enquote{App Client Squads}@@, entwickeln aber auch jene Features,
welche direkt auf der jeweiligen Plattform ausgeführt werden.\\
Reine @@latex:\enquote{App Client Squads}@@ erscheinen aufgrund der Projektgröße
nicht sinnvoll.
Die Tatsache, dass eine Corona-App entwickelt werden soll, lässt vermuten, dass
einige Mitarbeiter sich im Home Office befinden könnten.
Es ist daher zu beachten, dass die einzelnen Mitarbeiter sich in der Regel an
unterschiedlichen Orten befinden und die Kommunikation daher überwiegend online
stattfindet.
Dafür werden entsprechende Kanäle benötigt.
Die Online-Entwicklung wird für die Teams zunächst etwas ungewohnt sein.
Es muss daher damit gerechnet werden, dass die Produktivität zumindest zu Beginn
geringer ausfällt, als im Normalfall.\\
Die Skalierung geht bei diesem Anwendungsbeispiel in beide Richtungen:
Zunächst werden aufgrund der Lage recht viele Arbeitskräfte dafür bereitgestellt.
Man erhofft sich davon einen möglichst schnellen Einsatz der App.
Es handelt sich also um eine Skalierung des Ergebnisses.
Allerdings muss diese große Gruppe auch entsprechend koordiniert werden, damit
eine möglichst schnelle Entwicklung der App möglich ist.
Hierbei greift das Spotify Modell: Es sorgt für die nötige Struktur, die für ein
effizientes und produktives Vorgehen bei der Entwicklung benötigt wird.
Somit bewirkt die Anwendung des Modells eine Skalierung bzgl. der Produktivität.\\
Da bei diesem Anwendungsbeispiel eine thematische Unterteilung der Squads wenig
Sinn macht, kann auch beim Austausch von Best Practices davon ausgegangen
werden, dass die Chapter, die innerhalb eines Tribes ergeben, hauptsächlich aus
Mitarbeitern des selben Squads bestehen.
Der Austausch wird daher bei gegebenen Anlässen überwiegend direkt im jeweiligen Team stattfinden.
Hierbei ist zu beachten, dass Chapter über eine Chapter Lead verfügen, welche
für den Ablauf des Meetings verantwortlich ist.
So wird Chaos vermieden und das Meeting verläuft produktiv.
Gemeinsame Best Practices wird es am ehesten zwischen den beiden
plattformspezifischen Squads geben.
Dabei bilden sich Chapter im klassischen Sinne.\\
Damit die Squads ihr Vorgehen stetig verbessern können und produktiver werden,
erhalten sie Zugang zu einem agilen Coach.
Auch Improvementboards können in den Squads zum Einsatz  kommen.
Über deren Einsatz entscheidet jedoch jeder Squad aufgrund der Autonomie selbst.
Da es nur eine überschaubare Anzahl an Squads gibt, kann ein einziger agiler
Coach die Betreuung für alle Squads übernehmen.
Beim agilen Coach kann es sich ggf. um einen externen Mitarbeiter handeln.
** Nexus
In diesem Abschnitt wird dargestellt, wie ein fiktives Unternehmen vom Framework
Nexus profitieren kann.
Um die App, die unter normalen Umständen von einem kleinen Entwicklerteam
entwickeln werden könnte, gemäß den Anforderungen möglichst schnell auf den
Markt zu bringen, sollen etwa 40 Mitarbeiter für die Erstellung der App
miteinander arbeiten.
Die Anzahl der Teams ist dabei dynamisch, zu Beginn werden fünf Scrum Teams à
acht Personen eingeplant (inkl. Scrum Master).
Im Laufe der Entwicklung wird dabei die Anzahl der einzelnen Teammitgliedern den
aktuellen Anforderungen an das Team angepasst.
Den Teams wird dabei keine spezifische Profession zugeschrieben, z.B. /iOS App/,
wie es etwa bei Spotify der Fall ist.
Die Teams entsprechen exakt einem Scrum Team, wie es in einem nicht skalierten
Umfeld genutzt wird.
Jedes Team organisiert für sich selbst, wie seine Prozesse oder seine Planung
geschehen.
Weiterhin gibt es einen Product Owner und einen zusätzlichen Scrum Master.
Gemeinsam treten sie im /Nexus Integration Team/ auf, das seine Nutzer ansonsten
dynamisch festlegt.\\
Im ersten Schritt gilt es, ein möglichst umfassendes Product Backlog zu
erstellen um alle Anforderungen zu erfassen, etwa /Cross-Platform Application/.
Im nächsten Schritt findet das Nexus Refinement statt.
Zwischen den einzelnen Aufgaben, vorrangig die, die für den nächsten Sprint
vorgesehen sind, sollen Abhängigkeiten klar und deutlich dargestellt werden.
Dieser Schritt stellt eine komplexe Aufgabe dar, doch je gründlicher diese
Aufgabe erledigt wird umso produktiver können die einzelnen Teams im Endeffekt
arbeiten.
Es müssen aber nicht alle Abhängigkeiten bereits identifiziert werden: Wenn
wegen technischen Details weitere Abhängigkeiten entstehen, die während der
Erstellung des Backlogs zunächst nicht klar sind, können diese auch im Nexus
Sprint Planning durch die Teams identifiziert werden.\\
Beim gerade angesprochenen Nexus Sprint Planning treffen sich die Teams und
erstellen, unter Moderation eines Scrum Masters, ein Planning Board, auf dem die
aktuellen Aufgaben dargestellt werden.
Auch wenn das Unternehmen Schwierigkeiten hat, einen ausreichend großen Raum zur
Verfügung zu stellen (und in Corona Zeiten ist die Anforderung an Raum Größe
deutlich höher), sollte eine Lösung gefunden werden, wie sich alle Teams treffen
können.
Das Unternehmen hat sich in diesem Fall, aufgrund von Einschränkungen der
Räumlichkeiten und aufgrund der Tatsache, dass viele Mitarbeiter im Home Office
arbeiten, für eine Online Lösung entschieden.\\
Bereits identifizierte Abhängigkeiten zwischen Aufgaben werden gut sichtbar
dargestellt.
Die Teams können hier ihre Aufgaben für den nächsten Sprint wählen.
So können sie Fachwissen in ihrem Team zu einem bestimmten Bereich nutzen.
Die gewählten Aufgaben bereiten die Teams dann nach und identifizieren
eventuelle Abhängigkeiten.
Es kann sein, dass zu Beginn eines Projekts die Identifizierung von
Abhängigkeiten schwer fällt.
Dieser Prozess wird nach und nach durch Wissen über das Projekt und Wissen über
das Framework optimiert.\\
Zum Abschluss des Nexus Sprint Planning werden identifizierte Abhängigkeiten mit
den anderen Teams besprochen.
Danach beginnt der Arbeitszyklus, in dem jedes Team wie ein Scrum Team für sich
agiert.
Die einzige Kommunikation der Teams mit anderen Teams im Rahmen des Frameworks
und der Arbeit am Produkt ist das Nexus Daily Scrum.
Hier findet ein Wissensaustausch statt, der auch als Input für die einzelnen
Scrum Teams dient.
Für die Optimierung der Prozesse findet nach Ende des Sprints das /Nexus Daily
Scrum/ statt.
