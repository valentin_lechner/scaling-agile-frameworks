#+TITLE: A research chapter
#+LATEX_CLASS: elsarticle
#+LATEX_CLASS_OPTIONS: [authoryear,preprint,5p,times,twocolumn,11pt]
#+SETUPFILE: manuscript.setup
#+LATEX_HEADER:\bibliography{./ref/bibliography}
* Spotify
Das Spotify-Modell wurde 2012 von Henrik Kniberg und Anders Ivarsson unter dem
Titel @@latex:\enquote{Scaling Agile @ Spotify}@@ veröffentlicht. Es wird direkt
in einem Disclaimer darauf hingewiesen, dass diese Veröffentlichung nur eine
Momentaufnahme zeigt und das Spotify-Modell stetig weiterentwickelt und so
an die Bedürfnisse des Unternehmens hinter dem Streaming-Dienst angepasst
wird.\\
Dieses Kapitel dient zur Einführung in das Spotify-Modell und soll dessen
Funktionsweise aufzeigen. Um das Spotify-Modell erfolgreich anwenden zu können,
ist zusätzlich eine Auseinandersetzung mit der @@latex:\enquote{Spotify
Engineering Culture}@@ notwendig. Dies erfolgt in den darauf folgenden
Abschnitten.

** Einleitung Spotify
Das Spotify-Modell basiert auf Scrum und besteht aus Squads, Tribes, Chapters
und Guilds. Die untenstehende Abbildung ref:fig:spotify_squads_guilds zeigt
genau diese Bestandteile, welche im Folgenden erklärt werden.
#+BEGIN_EXPORT latex
\begin{figure}[hbtp]
\centering
\includegraphics[width=.9\linewidth]{../research-chapter/img/spotify_squads_guilds}
\caption[Aufbau des Spotify-Modells]{Aufbau des Spotify-Modells\footnotemark}
\label{fig:spotify_squads_guilds}
\end{figure}
\footnotetext{\cite[S. 1]{spotify_agile}}
#+END_EXPORT
Bei /Squads/ handelt es sich um autonome  Teams, welche in der Regel kleiner als
acht Personen sind. Autonom bezieht sich hierbei auf das Arbeiten innerhalb des
Squads sowie Entscheidungen hinsichtlich der Umsetzung. Dabei agiert der Squad
jedoch in einem Rahmen, welcher sich aus der jeweiligen /squad mission/ und der
Produktstrategie, sowie /short-term goals/ ergibt. Die /squad mission/ definiert
die übergeordnete Aufgabe des Squads, beispielsweise
@@latex:\enquote{iOS App}@@. Bei den /short-term goals/ handelt es sich um
Zwischenziele, welche vierteljährlich neu verhandelt werden. Jeder Squad
übernimmt jeweils den gesamten Implementierungsprozess, dieser reicht von
/Design/ über /Commit/ und /Deployment/ bis hin zur /Maintenance/. Die
Entwiucklung des Gesamtproduktes kann mit einer Jazz-Band verglichen werden.
Jeder Musiker ist autonom, dennoch müssen alle Musiker aufeinander hören und
sich auf den Song konzentrieren. Dies ist auch auf Squads übertragbar. Jeder
Squad trägt einen Teil zum Erfolg des Produkts und somit des Unternehmens bei.
Die Arbeitspriorisierung wird für jeden Squad vom /Product Owner/ vorgenommen.
Außerdem erhalten Squads Zugang zu einem /agilen Coach/.\\
Squads innerhalb eines Bereichs (z.B. Infrastruktur) werden zu /Tribes/
zusammengefasst. Tribes sollten kleiner  als  100  Mitarbeiter  sein,  um
weitere  Managementebenen  zu  verhindern. Die Führung eines Tribes wird vom
/Tribe-Lead/ übernommen. Vor allem bei   großen Projekten lassen sich
Abhängigkeiten zwischen Squads (u.a. auch unterschiedlicher Tribes) nicht
verhindern. Hierfür ist ein /scrum of scrums on demand/ vorgesehen.
Auch wenn jeder Squad andere Aufgaben übernimmt, sind die Herausforderungen oder
Entscheidungen oftmals identisch oder ähnlich. Daher gibt es eine weitere Ebene
zum Austausch mit Experten auf dem jeweiligen Gebiet. Eine solche Expertengruppe
innerhalb eines Tribes wird als /Chapter/ bezeichnet. Die Chapter treffen sich
regelmäßig um Wissen und Erfahrungen auszutauschen. Sie testen neue Tools oder
erarbeiten neue  Mechanismen und Best-Practices, von denen alle Squads
profitieren. Die /Chapter-Lead/ wird von einem Mitglied des Chapters übernommen.
Dadurch ist die Leitung selbst in aktuelle Themen und Probleme involviert.
Chapter müssen keineswegs gleichmäßig über die Squads verteilt sein.\\
Neben den Chaptern gibt es zudem /Guilds/. Es handelt sich dabei um eine
unternehmensweite /Community of Interests/, die in der Regel alle
Kompetenzträger eines Bereichs (z.B. der Bereich Webentwicklung) und zusätzlich
weitere interessierte Mitarbeiter, umfasst. Guilds dienen, ähnlich den Chapters,
ebenfalls dem Austausch von Wissen und Erfahrungen sowie der Vorstellung neuer
Werkzeuge. Hier findet der Austausch aber unternehmensweit statt. Die
Koordinierung einer Guild wird vom jeweiligen /Guild Coordinator/ übernommen.

** Spotify Engineering Culture<<sec:engcult>>
In diesem Abschnitt erfolgt die Vorstellung der Spotify Engineering Culture, von
welcher das Spotify-Modell maßgeblich geprägt wird. Es basiert auf den von
Spotify veröffentlichten Videos zur Unternehmenskultur. Im weiteren Verlauf der
Arbeit wird sich die Spotify Engineering Culture als essentiell zur
erfolgreichen Anwendung des Spotify-Modells in einem Unternehmen erweisen.\\
Das Spotify-Modell entstand durch Abwandlungen von Scrum, die mit dem Anstieg
der Anzahl an Entwicklungsteams im Unternehmen vorgenommen wurden.
Es begann damit, das agile Vorgehen über Scrum zu stellen und somit agile
Prinzipien über bestimmte Vorgehen zu heben. Zudem wurde der /Scrum Master/ zum
/Agile Coach/ umbenannt, um diesen nicht als Lehrmeister, sondern als Diener des
Teams darzustellen. In diesem Zuge wird auch die Autonomie eines /Squads/
hervorgehoben, welcher wiederum aus dem /Scrum Team/ hervorgeht. Spotify
bewertet Vorgehen und Regeln offenbar über die Agilität und nimmt dabei ggf.
Anpassungen zur Steigerung der Agilität vor. Die Anpassung eines etablierten
Vorgehensmodells wie Scrum lässt darauf schließen, dass Spotify nach Innovation
strebt und dabei selbst experimentierfreudig ist.\\
Spotify fördert Teamarbeit durch entsprechende räumliche Einteilung der
Bürogebäude: Typischerweise hat jeder Squad eine /desk area/, eine /lounge area/
und einen /huddle room/. Typischerweise sind in allen diesen Räumen Whiteboards
anzutreffen. Dies fördert Autonomie und Motivation der Mitarbeiter und
ermöglicht kollaboratives Arbeiten.
Zudem führt die Autonomie zu schnelleren Entscheidungen, da diese innerhalb
eines Squads getroffen werden. Diese müssen stets mit Produktstrategien,
Unternehmensprioritäten und anderen Squads im Einklang stehen und mit dem
übergeordneten Ziel des Unternehmens vereinbar sein. Die Autonomie wird durch
die Führung hierbei in keinster Weise beschränkt, vielmehr werden Probleme und
Aufgaben von der Führungskraft an das Team kommuniziert und die Entscheidung
bzgl. Lösung und Umsetzung dem Squad überlassen. Das Vertrauen in die
Mitarbeiter spielt in der Engineering Culture eine wichtige Rolle, was sich
positiv auf deren Motivation auswirkt. Allerdings besteht theoretisch die
Möglichkeit, dass dieses Vertrauen ausgenutzt wird. Bei Spotify gibt es hierbei
aber keine Probleme, jedoch stellt der @@latex:\enquote{richtige}@@ Führungsstil
einen wichtigen Aspekt dar, wenn es um die Einführung des Spotify-Modells geht.
Spotify wertet das Vertrauen in die Mitarbeiter höher als die Kontrolle und
begründet dies damit, dass nur Mitarbeiter eingestellt werden, denen man auch
vertraut. Zudem sieht Spotify keine Bestrafungen bei Fehlern vor um Mitarbeiter
dazu zu ermutigen, etwas auszuprobieren und somit Innovation zu erfahren.\\
Auch führt Autonomie dazu, dass es keine festgelegten Standards gibt: Jeder
Squad kann die Vorgehen zur Erfüllung seiner Aufgaben selbst wählen, was dazu
führt, dass fünf verschiedene Squads die gleiche Aufgabe mit fünf
unterschiedlichen Vorgehen lösen könnten. Unter den Squads findet auch ein
Austausch statt, wodurch diese ihr methodisches Vorgehen oder den Einsatz von
Werkzeugen optimieren. Erscheint beispielsweise ein Tool für alle Teams
sinnvoll, werden alle Teams dieses einsetzen. Die Definition eines Standard ist
dafür nicht notwendig.\\
Spotify verfügt über ein /Internal Open-Source Model/, welches vorsieht, dass
ein System von jenem Squad verwaltet wird, welcher zuvor die Entwicklung
übernommen hat. Werden jedoch dringend Änderungen benötigt und der zuständige
Squad ist mit anderen Projekten ausgelastet, können die benötigten Änderungen
auch von anderen Squads vorgenommen werden. Das Deployment erfolgt dennoch über
den eigentlich zuständigen Squad, da man sich von dessen Code-Review eine
bessere Qualitätskontrolle erhofft. Die Architektur besteht aus vielen
verschiedenen Systemen, welche unabhängig voneinander entwickelt wurden. Jedes
System repräsentiert dabei ein Unternehmensinteresse und kann über eine
definierte Schnittstelle verwendet werden.\\
Wie bereits hervorging, ist Spotify die Motivation seiner Mitarbeiter sehr
wichtig. Teamwork wird förmlich gelebt, einzelne @@latex:\enquote{Egos}@@ haben
bei Spotify nichts zu suchen. Das spiegelt sich auch darin nieder, dass
Mitarbeiter sich untereinander gerne helfen. Spotify strebt an, dass Mitarbeiter
gerne und zufrieden bei Spotify arbeiten. Um das zu sichern, werden Mitarbeiter
nach Verbesserungsvorschlägen befragt. Durch Chapters werden Möglichkeiten zum
Wissensaustausch und dem Lösen von Squadübergreifenden Problemen geschaffen. In
Abbildung ref:fig:spotifyengcult1 und ref:fig:spotifyengcult2 (s. Anhang) werden
diese recht strukturiert dargestellt. In Wirklichkeit sind diese aber
unstrukturiert über die Squads verteilt. In den Guilds wird eine /Community of
Interest/ einbezogen, also auch Mitarbeiter aus anderen Bereichen.
Entscheidungen werden in dieser Gruppe getroffen, nicht von einer einzelnen
Person.\\
Hinsichtlich Versions-Releases spricht sich Spotify für kleine regelmäßige
Releases aus und geht dabei vom @@latex:\enquote{Drama}@@ zur
@@latex:\enquote{Routine}@@ über. Als @@latex:\enquote{Drama}@@ wird in diesem
Zusammenhang der Teufelskreis bezeichnet, wenn Releases schwer fallen und daher
selten durchgeführt werden. Jedoch werden Releases umso größer und fallen
schwerer, wenn diese selten durchgeführt werden. Im Gegensatz dazu wird als
@@latex:\enquote{Routine}@@ bezeichnet, wenn Releases leicht fallen und daher
oft durchgeführt werden und leichter fallen. Auch wurde im Hinblick auf die
steigende Anzahl an Teams die monolithische Architektur in kleine Services
aufgeteilt. Das hat den Vorteil, dass Teams unabhängig voneinander Releases
ihrer Services durchführen können. Beim Monolithen hingegen müssen alle Teams
gemeinsam das Release durchführen und sich hierzu eng absprechen, was mit
steigender Anzahl an Teams zunehmend ineffizienter, komplizierter und somit
fehleranfälliger wird. Zudem wurden die Squads in /Client App Squads/, /Feature
Squads/ und /Infrastructure Squads/ aufgeteilt. Feature Squads übernehmen je ein
Feature von Entwurf über Entwicklung und Release bis zur Wartung. Client App
Squads haben die Aufgabe, Releases für die jeweilige Plattform möglichst einfach
zu machen. Infrastructure Squads sollen den anderen Squads die Arbeit durch das
Bereitstellen von Tools für /Continuous Delivery/, /A/B Testing/ oder Monitoring
möglichst leicht machen. Spotify bezeichnet dies als /Self Service Model/ und
vergleicht es mit einem Buffet, wobei die Restaurantmitarbeiter nicht bedienen,
jedoch ermöglichen, sich selbst zu bedienen. Im übertragenen Sinne ermöglichen
und unterstützen Infrastructure Squads sowohl Client App Squads als auch Feature
Squads bei Releases, ebenso ermöglichen und unterstützen Client App Squads die
Feature Squads. Releases werden durch /Release Trains/ und /Feature Toggles/
durchgeführt. Hierbei werden die Features in bestimmten Abständen vom Release
Train abgeholt. Das betrifft auch Features, die noch nicht fertiggestellt sind.
Mittels /Feature Toggles/ werden diese versteckt. So lassen sich frühzeitig
Integrationsprobleme erkennen und es werden weniger Code-Branches benötigt.\\
Das Unternehmen platziert sich sehr tolerant gegenüber Fehlern:\\
#+BEGIN_fancyquotes
We aim to make mistakes faster than anyone else
   --- Daniel Ek, CEO Spotify [[footcite:spot_eng_cult_2][Vgl. 1:00min]]
#+END_fancyquotes

Es wird also angestrebt, Fehler möglichst schnell zu begehen, um so besser zu
werden, indem man daraus lernt. Dies ermöglicht eine schnellere Entwicklung und
identifiziert Probleme möglichst früh. Spotify ist eher an der schnellen
Erholung und dem Lernen aus Fehlern als an der Fehlerverhinderung interessiert.
Typischerweise werden Fehler besprochen, wobei keine Schuldzuweisung
stattfindet, sondern wie aus dem Fehler gelernt werden kann. Der Austausch
bezüglich geschehener Fehler ist für Spotify dabei sehr wichtig. Generell findet
bei Spotify ein sogenanntes /Continuous Improvement/ statt. Beispielsweise wird
innerhalb von Squads regelmäßig besprochen, was gut läuft und was verbessert
werden kann oder wo Probleme auftreten. Verbesserungsmöglichkeiten werden so
häufig auf Squadebene identifiziert und von höheren Ebenen unterstützt. In der
Regel verfügen Squads über sog. /Improvement Boards/. Zudem erstellt jeder Squad
eine eigene /Definition of Awesome/, die anzustreben ist. Hierzu wird im
/Improvement Board/ eingeordnet, wo sich der Squad gerade befindet und wo sich
der Squad einordnen möchte. Auch wird hier geplant, wie der Squad im nächsten
Schritt seinem Ziel, @@latex:\enquote{Awesome}@@ zu werden, ein Stückchen näher
kommt. Spotify begegnet aufgrund seines schnellen Wachstums ständig neuen
Problemen, die vor allem durch stetige Verbesserungen im Squad Umfeld gemeistert
werden. Damit scheint das Unternehmen ganz gut zurecht zu kommen und führt dies
auf die Unternehmenskultur zurück: @@latex:\enquote{Healthy Culture heals Broken
Process}@@.[[footcite:spot_eng_cult_2][12:12min]] Um die Verbesserungen
voranzutreiben gibt es /Culture-focused Roles/, zu denen auch die /Agile
Coaches/ gehören. Auch werden teils reelle Probleme im Rahmen eines einwöchigen
Boot Camps gelöst. Das Boot Camp gleicht dabei einem temporären Squad.\\
Außerdem verfügt Spotify über Möglichkeiten, das Ausmaß eines Fehlers zu
beschränken. So ermöglicht beispielsweise die verteilte Architektur, dass bei
einem Fehler im Musik Player nur der Teil mit dem Fehler nicht mehr funktioniert
und der Rest des Players nicht vom Fehler betroffen ist. Auch werden neue
Features schrittweise verteilt, sodass nicht direkt alle Nutzer vom Fehler
betroffen sind. Kommt es daher zum Fehler, so ist nur ein kleiner Teil des
Gesamtsystems und auch nur ein kleiner Teil der Nutzer für kurze Zeit davon
betroffen. Squads können dadurch gut experimentieren und schnell lernen. Da
Fehler schnell beseitigt werden können, versucht man nicht, alles zu
kontrollieren.\\
Die Produktentwicklung organisiert sich bei Spotify nach dem Motto
@@latex:\enquote{Think it. Build it. Ship it. Tweak it.}@@ Das größte Risiko
sieht Spotify darin, ein Produkt oder Feature zu entwickeln, welches vom Nutzer
gar nicht gewollt oder benötigt wird. Daher wird zunächst analysiert, was die
Nutzer wollen, um daraus eine Beschreibung und Prototypen entwickeln zu können.
Im Anschluss wird ein /Minimal Viable Product/ (MVP) entwickelt, welches gerade
so die Beschreibung abdeckt und keineswegs das vollständige Produkt darstellt.
Es  entsteht  ein  Kreislauf,  welcher  mit  dem  Release an  einen  kleinen
Nutzerkreis beginnt. Feedback  und  Erfahrungen  der  Nutzer  werden  dann
analysiert,  das  Produkt  optimiert  und schließlich  wieder ausgeliefert. Erst
wenn das Feedback der @@latex:\enquote{Testnutzer}@@ es zulässt, werden  die
Änderungen  an  alle  Nutzer  verteilt. Die Wirkung auf den Nutzer ist dabei
wichtiger, als dass die Änderung schnell bei diesem ankommt.
Auch  die  Produktentwicklung  konzentriert  sich  mehr  auf  Innovation  als
auf  Vorhersagbarkeit.  Es  ist daher meistens nicht möglich, vorherzusagen,
wann ein Release stattfindet. Änderungen sollen einen Mehrwert bringen und nicht
etwa den Plan von jemandem erfüllen. Damit  ein  derartiger  Mehrwert  gegeben
ist, werden  stets  Ideen  benötigt.  Spotify gewährt  seinen Mitarbeitern
daher  etwa  10%  ihrer  Arbeitszeit  als  @@latex:\enquote{Hack  Time}@@ -
Zeit, in welcher  sie experimentieren können und neue Techniken austesten. Ob
etwas Sinnvolles dabei herauskommt, spielt keine Rolle. Es wird jedoch darauf
vertraut, dass durch dieses @@latex:\enquote{Ausprobieren}@@ neue Ideen
hervordringen,  welche  dann  einen Mehrwert darstellen. Zusätzlich macht es
Spaß und motiviert die Mitarbeiter. Neben der @@latex:\enquote{Hack Time}@@ gibt
es zudem zweimal im Jahr @@latex:\enquote{Spotify Hack Weeks}@@, welche jeweils
eine ganze Woche umfassen und nach dem Motto
@@latex:\enquote{Make cool things  real!}@@[[footcite:spot_eng_cult_2][6:54]]
stattfinden. Am Ende einer
@@latex:\enquote{Hack Week}@@ findet zudem eine Demo mit anschließender Party
statt. Das steigert  die Motivation der Mitarbeiter, fördert das Teambuilding
und bringt  zumindest coole Ideen hervor. Spotify verfolgt  den Ansatz, dass
Menschen von Natur aus Entdecker sind und versucht so Innovation zu fördern.
Dies  spiegelt  sich  auch in  einer  sehr  experimentierfreundlichen
Unternehmenskultur  nieder. So werden Entscheidungen nicht durch Meinungen, Ego
oder Autoritätengetroffen. Es wird schlichtweg einfach  ausprobiert,  verglichen
und schließlich auf dieser experimentellen Grundlage entschieden.
Wenn etwas funktioniert und sinnvoll erscheint, wird es behalten, anderenfalls
verworfen. Für Spotify funktionieren beispielsweise
@@latex:\enquote{Time reports}@@,
@@latex:\enquote{Handoffs}@@,
@@latex:\enquote{separate test teams or test phases}@@,
@@latex:\enquote{Task estimates}@@,
@@latex:\enquote{Useless meetings}@@ sowie
@@latex:\enquote{Corporate BS}@@
nicht. Dies wird jedoch zu vielen Unternehmenskulturen dazugehören und ist daher
besonders zu beachten, wenn das Spotify-Modell angewandt werden soll. Auch
großen Projekten steht Spotify kritisch gegenüber, da diese mit einem hohen
Risiko verbunden
sind. Es  wird  daher  versucht, die  Notwendigkeit  für große  Projekte  zu
minimieren. Aber  auch  wenn große Projekte notwendig sind und die sich
ergebenden Vorteile den Risiken überwiegen, hat Spotify ein Vorgehen hierfür: So
wird der Fortschritt visuell festgehalten (z.B. auf einem Whiteboard) und es
findet ein @@latex:\enquote{Daily Sync}@@ Meeting statt, in welchem alle
beteiligten Squads vertreten sind. Außerdem gibt es  eine  wöchentliche  Demo,
in  welcher  alle  Teile  zusammengeführt und mit  den Stakeholdern besprochen
werden. Durch die regelmäßigen Absprachen wird das Risiko minimiert. Zudem
erachtet Spotify bei großen Projekten eine Führungsgruppe bestehend aus /Tech
lead/, /Product lead/ und ggf. auch /Design lead/ als sinnvoll. Jedoch sieht
Kniberg in der Durchführung von großen Projekten noch viel
Verbesserungspotential. Spotify steht,  aufgrund  seines  schnellen  Wachstums,
ständig  vor  dem  Problem,  in  Chaos  oder Bürokratie  zu  verfallen.  Hierbei
sieht  Spotify  seine /Agile Culture/ als Balance zwischen Chaos und Bürokratie,
wobei Chaos  als  das  kleinere  Übel  gesehen  wird.  Spotify  will  mit
möglichst  wenig Bürokratie das totale Chaos verhindern. Zuletzt  beschreibt
Kniberg,  dass  die  Mitarbeiter  die  Unternehmenskultur prägen und  man  das
Verhalten vorleben soll, welches man gerne sehen würde. Zusammenfassend lässt
sich sagen, dass Spotify sehr innovativ ist, auf motivierte Mitarbeiter setzt und
dies durch einen sehr liberalen Führungsstil erreicht. Weiter ist für Spotify
die Teamarbeit sowie die Autonomie der Entwicklerteams wichtig.
** Vergleich und Abgrenzung zu herkömmlichen Vorgehensmodellen
Im Spotify-Modell ist klar zu erkennen, dass sich dieses aus Scrum heraus
entwickelte. So gibt es bei beiden Modellen einen Product Owner und die Rolle
des Scrum Masters, wobei diese bei Spotify etwas flexibler durch den Agile Coach
vertreten wird. Scrum Teams weisen zudem die gleiche Größe auf, wie Squads bei
Spotify und beide Modelle verfügen über ein Product Backlog. Man könnte dazu
tendieren, das Spotify-Modell als modifiziertes Scrum zu bezeichnen,
allerdings wäre diese Bezeichnung irreführend und gemäß der Entwickler von
Scrum nicht zulässig. In der Schlussbemerkung des
@@latex:\enquote{Scrum Guides}@@ schreiben Schwaber und Sutherland:\\
#+BEGIN_fancyquotes
Die Rollen, Artefakte, Ereignisse und Regeln von Scrum sind unveränderlich. Es
ist zwar möglich, nur Teile von Scrum einzusetzen – das Ergebnis ist dann aber
nicht  Scrum.
   --- Ken Schwaber, Jeff Sutherland[[footcite:ScrumGuide_2017_DE][S. 17]]
#+END_fancyquotes

Die  Bezeichnung Scrum kann  also  für  das Vorgehen bei  Spotify nicht  mehr
genutzt werden. Es liegt nahe, dass die Bezeichnung „Spotify-Modell“ aufkam, als
Spotify das Vorgehen vorstellte. Dieses Modell besteht  aus einem  Rahmen  zur
Skalierung  von Scrum, einer Reihe von Rules und Practices, sowie einer
Unternehmenskultur, welche einen Rahmen um das Modell bildet und dabei alles
zusammenhält. Die Rules und Practices ergeben sich aus einer Art
@@latex:\enquote{agilen Baukasten}@@ und der Unternehmenskultur. Spotify strebt
ein  möglichst  agiles  Vorgehen  an  und prüft,  wie  dies  mit  der
Unternehmenskultur vereinbar  ist. Dabei ist  zu  beachten,  dass  die
Unternehmenskultur  von  Spotify  durch  das  starke Wachstum des
Streamingdienstes geprägt ist. Die Unternehmenskultur erinnert an ein Startup,
wobei den  Mitarbeitern sehr große  Freiräume gewährt werden  und sich die
Bürokratie  auf  ein  Minimum beschränkt. Die Unternehmenskultur lebt vom
Austausch der Mitarbeiter, dies kommt durch Chapters und Guildes hervor, beginnt
aber bereits bei der räumlichen Einteilung der Bürogebäude. Auch ist die
Unternehmenskultur sehr auf Innovation und Motivation der Mitarbeiter
ausgerichtet. Der  Rahmen  zur  Skalierung  von Scrum wird  durch  Tribes
realisiert, wobei die  Tribe  Lead  und die Product Owner (der Squads) die
Skalierung ermöglichen. Durch Chapters wird zudem der Austausch von  Wissen  und
Erfahrungen innerhalb  des  Tribes ermöglicht. Auch  wird ein
@@latex:\enquote{scrum  of  scrums  on demand}@@ vorgesehen. So kann bei
Bedarf eine Synchronisation   auf Squad- oder   Tribe-Ebene stattfinden. Das
Spotify-Modell kann aufgrund der
#+BEGIN_EXPORT latex
\enquote{Unveränderlichkeit von Scrum}\footcite[S. 17]{ScrumGuide_2017_DE}
#+END_EXPORT
als  eigenes  Modell gesehen
werden.  Es  setzt  sich  zusammen aus veränderten Bestandteilen  von  Scrum,
einem Rahmen  zur Skalierung  sowie  Rules  und  Best  Practices,  welche  aus
der  Unternehmenskultur  abgeleitet  werden können.
** Einführung des Spotify-Modells in einem Unternehmen
Die Einführung des Spotify-Modells an sich erscheint recht einfach,
jedoch ist es sehr stark mit der Unternehmenskultur von Spotify verbunden,
was maßgeblich zum Erfolg und Scheitern des Modells beiträgt. Die   Hürde   zur
erfolgreichen   Einführung   liegt   also in   der   jeweils   anzutreffenden
Unternehmenskultur. In diesem Kapitel erfolgt daher zunächst eine
Auseinandersetzung mit Eigenschaften der Spotify Engineering  Culture,  welche
das Spotify-Modell förmlich  stützen.  Daraus können im  Anschluss wiederum
Gegensätze  abgeleitet  werden,  welche negative  Einflüsse  auf  das Modell
haben  oder es sogar  scheitern lassen. Aus  diesen  Eigenschaften  lässt  sich
zuletzt eine  Art @@latex:\enquote{Fahrplan}@@ zur erfolgreichen Einführung und
Anwendung des Spotify-Modells erarbeiten.\\  Wie  bereits  in  Kapitel
[[sec:engcult]] zusammengefasst, ist Spotifys  Unternehmenskultur geprägt  von
einem liberalen  Führungsstil, motivierten  Mitarbeitern,  Teamarbeit, autonomen
Entwicklerteams sowie Innovation. Der  Führungsstil  ist  dabei besonders zu
betrachten. Spotify  pflegt  einen  sehr  liberalen Führungsstil und gewährt
seinen  Mitarbeitern  dadurch viele  Freiräume. Auch  sieht  der  liberale
Führungsstil  vor,  dass  die  Führungskräfte die Arbeit  priorisieren  und an
die Teams kommunizieren. Jedoch obliegt  es  den  Teams,  die bestmögliche
Lösung  zu  finden  und  umzusetzen. Die  Mitarbeiter werden durch  die
Freiräume  und  Autonomie motiviert.  Zudem  fördert  der liberale  Führungsstil
Innovation, indem  Mitarbeiter zusätzliche  Freiräume  erhalten,  um Dinge
auszuprobieren. Auch werden Fehler als Chance gesehen, etwas zu lernen. Somit
findet keine Bestrafung statt, wenn jemand einen  Fehler macht.  Dass die
Teamarbeit  mit motivierten Mitarbeitern  besser funktioniert,  liegt  auf der
Hand. Oftmals  ist  der  Führungsstil  eher  autoritär  geprägt:  Hierbei müsste
dieser  zumindest insoweit angepasst  werden,  dass die  Entwicklerteams
möglichst  autonom sind. Auch  muss  sichergestellt werden,  dass  die
Mitarbeiter  ausreichend  motiviert  sind, um  vernünftige  Teamarbeit zu
leisten. Werden  Freiräume  zum @@latex:\enquote{Ausprobieren}@@ eingeschränkt,
ergeben  sich  Einbußen  bzgl.  der  Motivation und der Innovation. Zudem sollte
darüber nachgedacht werden, wie aus Fehlern gelernt werden kann, anstatt
einzelne  Mitarbeiter für  Fehler verantwortlich  zu  machen.\\
Es steht außer Frage, dass das Spotify-Modell am  besten bei Spotify selbst
funktioniert.  Jedoch kann  dieses  auch von  anderen Unternehmen   erfolgreich
angewandt   werden,   wenn die bestehende   Unternehmenskultur   der
Entfaltung  des Modells nicht zu  sehr entgegen  steht. Hierbei erfolgt die
Einteilung  in  Squads  und Tribes, wobei sich Chapter und Guildes quasi von
selbst bilden. Vor allem der Führungsstil muss dann aber  mit  den
Gepflogenheiten  des  Modells  im  Einklang  stehen. Dies  hängt  immer  vom
jeweiligen Unternehmen  selbst  ab, ein  erfolgsversprechender Fahrplan  für
die  Einführung  des  Spotify-Modells kann daher nicht pauschal erarbeitet
werden. Jedoch sollte aus diesem Kapitel hervorgehen, auf was bei der Einführung
besonders geachtet werden muss. Prinzipiell wird sich das Modell eher leicht auf
Unternehmen übertragen lassen, welche in kurzer Zeit sehr stark gewachsen sind
oder einem Startup ähneln, da in diesen Unternehmen genau jene Entwicklung
stattfindet, aus welcher das Spotify-Modell entstand. Bei anderen Unternehmen
sind ggf. viele   Änderungen   erforderlich,   wobei   dann abgewogen   werden
sollte,   ob   das   Spotify-Modell tatsächlich  das  richtige Vorgehensmodell
für  dieses Unternehmen  ist. Derzeit  scheinen  vermehrt Banken das
Spotify-Modell für sich zu entdecken.footcite:spotify_banken
