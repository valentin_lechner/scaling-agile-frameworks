#+TITLE: Agilität im Großen
#+latex_class: mimosis
#+latex_class_options: [bibliography=totoc,paper=a4,oneside,12pt,ngerman,appendixprefix,openany]
#+latex_header: \KOMAoptions{headings=small,fontsize=12}
#+setupfile: ./thesis.setup
#+latex_header: \bibliography{./thesis}
#+EXCLUDE_TAGS: journal noexport
#+language: de
* Acronym and Glossary :ignore:

# {{{glossaryentry(LaTeX,\LaTeX,A document preparation system,LaTeX)}}}
# {{{glossaryentry(Real Numbers,$\real$,The set of Real numbers,Real Numbers)}}}

# #+latex_header: \makeglossaries

* Frontmatter :ignore:
#+BEGIN_EXPORT latex
\frontmatter
#+END_EXPORT

#+INCLUDE: ./title.org

* Mainmatter :ignore:

#+BEGIN_EXPORT latex
\tableofcontents
\listoffigures
\listoftables
\mainmatter
#+END_EXPORT

* Agile Methoden in der Praxis :ignore:


#+INCLUDE: "./introduction/introduction.org" :lines "6-"

* Grundlagen agiler Methoden :ignore:

#+INCLUDE: "../research-chapter/grundlagen.org" :lines "6-"

* Skalierte agile Methoden :ignore:

#+INCLUDE: "../research-chapter/skalierung.org" :lines "6-"

* SAFe :ignore:

#+INCLUDE: "../research-chapter/safe.org" :lines "6-"

* LeSS :ignore:

#+INCLUDE: "../research-chapter/less.org" :lines "6-"

* Spotify :ignore:

#+INCLUDE: "../research-chapter/spotify.org" :lines "6-"

* Nexus :ignore:

#+INCLUDE: "../research-chapter/nexus.org" :lines "6-"

* Anwendungsbeispiel :ignore:

#+INCLUDE: "../research-chapter/anwendungsbeispiel.org" :lines "6-"

* Generaldebatte :ignore:

#+INCLUDE: "../research-chapter/debate.org" :lines "6-"

* Fazit :ignore:

#+INCLUDE: "../research-chapter/fazit.org" :lines "6-"
* Appendix :ignore:

#+BEGIN_EXPORT latex
% This ensures that the subsequent sections are being included as root
% items in the bookmark structure of your PDF reader.
%\bookmarksetup{startatroot}
% \backmatter
% \begingroup
%     \let\clearpage\relax
%     \glsaddall
%     \printglossary[type=\acronymtype]
%     \newpage
%     \printglossary
% \endgroup
% \printindex
#+END_EXPORT

#+INCLUDE: "./appendix.org"


* Bibliography :ignore:

#+BEGIN_EXPORT latex
\bookmarksetup{startatroot}
\backmatter
\printbibliography
#+END_EXPORT


* Build :noexport:

Bind derivatives change variable values *locally* on export.

These two are because I'm defining the title and toc manually using latex, so I don't want org-latex to take care of that.
#+BIND: org-latex-title-command ""
#+BIND: org-latex-toc-command ""
This is so that src code blocks get src highlighting from the minted package.
#+BIND: org-latex-listings minted
This beautifies table borders. It will only work if the booktabs package is loaded, which I do in the setup file.
#+BIND: org-latex-tables-booktabs t
And this configuration increases the default width of images, so that they are larger and more readable on print.
#+BIND: org-latex-image-default-width ".97\\linewidth"

# Local Variables:
# mode: org
# org-export-allow-bind-keywords: t
# End:
